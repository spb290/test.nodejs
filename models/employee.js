'use strict';
const {Model} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Employee extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate({Department}) {
            // define association here
            //this.belongsTo(Department, {foreignKey: 'departmentId'})
        }

        toJSON() {
            return super.toJSON(
                {
                    ...this.get(), id: undefined
                });
        }
    };
    Employee.init({
        fio: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notNull: {msg: 'Не указано ФИО сотрудника'},
                notEmpty: {msg: 'ФИО сотрудника не может быть пустым'}
            }
        },
        departmentId:{
            type: DataTypes.INTEGER,
            allowNull: true
        }
    }, {
        sequelize,
        modelName: 'Employee',
    });
    return Employee;
};