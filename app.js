const express = require('express')
const {sequelize, Employee, Department, Op} = require('./models')

const app = express()
app.use(express.json())

/************
 employee
 ************/
//get all employees
app.get('/employees', async (req, res) => {
    try {
        const employees = await Employee.findAll()
        return res.json(employees)
    } catch (err) {
        console.log(err)
        return res.status(500).json(err)
    }
})
//get employee by id
app.get('/employees/:id', async (req, res) => {
    const id = req.params.id
    try {
        const employee = await Employee.findOne({
            where: {id}
        })
        return res.json(employee)
    } catch (err) {
        console.log(err)
        return res.status(500).json(err)
    }
})
//create employee
app.post('/employees', async (req, res) => {
    const {fio} = req.body
    try {
        const employee = await Employee.create({fio})
        return res.json(employee)
    } catch (err) {
        console.log(err)
        return res.status(500).json(err)
    }
})
//delete employee
app.delete('/employees/:id', async (req, res) => {
    const id = req.params.id
    try {
        const employee = await Employee.findOne({ where: {id} })
        await employee.destroy()
        return res.json({message: 'Сотрудник удален!'})
    } catch (err) {
        console.log(err)
        return res.status(500).json(err)
    }
})
//update employee
app.put('/employees/:id', async (req, res) => {
    const id = req.params.id
    const {fio} = req.body
    try {
        const employee = await Employee.findOne({ where: {id} })
        employee.fio = fio
        await employee.save()

        return res.json({message: 'Сотрудник обновлен!'})
    } catch (err) {
        console.log(err)
        return res.status(500).json(err)
    }
})

//employee attach to department
app.put('/departments/:department_id/attach-employee/:employee_id', async (req, res) => {

    const id = req.params.employee_id
    const department_id = req.params.department_id

    try {
        const employee = await Employee.findOne({ where: {id} })
        employee.departmentId = department_id
        await employee.save()

        return res.json({message: 'Сотрудник добавлен в отдел...!'})
    } catch (err) {
        console.log(err)
        return res.status(500).json(err)
    }
})

//employee unattached from department
app.put('/departments/:department_id/unattached-employee/:employee_id', async (req, res) => {
    const id = req.params.employee_id
    const department_id = req.params.department_id

    try {
        const employee = await Employee.findOne({ where: {id} })
        employee.departmentId = null
        await employee.save()

        return res.json({message: 'Сотрудник выгнан с позором из отдела!'})
    } catch (err) {
        console.log(err)
        return res.status(500).json(err)
    }
})
/************
 department
 ***********/
//get all departments
app.get('/departments', async (req, res) => {
    try {
        const department = await Department.findAll({include: 'employees'})
        return res.json(department)
    } catch (err) {
        console.log(err)
        return res.status(500).json(err)
    }
})
//get department by id
app.get('/departments/:id', async (req, res) => {
    const id = req.params.id
    try {
        const department = await Department.findOne({
            where: {id},
            include: 'employees'
        })
        return res.json(department)
    } catch (err) {
        console.log(err)
        return res.status(500).json(err)
    }
})
//create department
app.post('/departments', async (req, res) => {
    const {name} = req.body
    try {
        const department = await Department.create({name})
        return res.json(department)
    } catch (err) {
        console.log(err)
        return res.status(500).json(err)
    }
})

//delete department
app.delete('/departments/:id', async (req, res) => {
    const id = req.params.id
    try {
        const department = await Department.findOne({ where: {id} })
        await department.destroy()
        return res.json({message: 'Отдел удален!'})
    } catch (err) {
        console.log(err)
        return res.status(500).json(err)
    }
})
//update department
app.put('/departments/:id', async (req, res) => {
    const id = req.params.id
    const {name} = req.body
    try {
        const department = await Department.findOne({ where: {id} })
        department.name = name
        await department.save()

        return res.json({message: 'Отдел обновлен!'})
    } catch (err) {
        console.log(err)
        return res.status(500).json(err)
    }
})

/*
Search by name employee
 */
app.get('/departments/search/:term', async (req,res)=>{
    const term = req.params.term

    try {
        const departments = await Department.findAll(

            {
                where: {
                    name: {
                        [Op.like]: term+'%'
                    }
                }
            },
            {include: 'employees'},
        )
        return res.json(departments)
    } catch (err) {
        console.log(err)
        return res.status(500).json(err)
    }
})

app.listen({port: 5000}, async () => {
    console.log('Server up on http://localhost:5000')
    //await sequelize.sync({force: true})
    await sequelize.authenticate()
    console.log('Db connected!')
})

